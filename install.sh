#!/bin/bash

while getopts "ai:hv" options; do
  case ${options} in
    a)
      all=true;;
    i)
      install_only="${OPTARG}";;
    h)
      printHelp;
      exit 0;;
  esac
done


############################################
#################Setup######################
############################################
# Config Folder
configsFolder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Config Root (folder above)
configsRoot="$( dirname ${configsFolder} )"
# Namespace for git installation
git="/usr/bin/git"

############################################
##################Tmux######################
############################################
function tmux {
  echo "Configuring Tmux"
  ln -sf ${configsFolder}/tmux/tmux.conf $HOME/.tmux.conf
  ln -sf ${configsFolder}/tmux/tmux.conf.local $HOME/.tmux.conf.local
}
############################################
###################Zsh######################
############################################
function zsh {
  echo "Installing zsh plugins"
  mkdir -p ${configsRoot}/zsh-plugins
  # Installing zsh syntax hightlighting
  if [[ ! -d ${configsRoot}/zsh-plugins/zsh-syntax-highlighting ]]; then
    $git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${configsRoot}/zsh-plugins/zsh-syntax-highlighting
  fi
  # Installing oh my zsh 
  if [[ ! -d ${configsRoot}/zsh-plugins/oh-my-zsh ]]; then
    $git clone https://github.com/ohmyzsh/ohmyzsh.git ${configsRoot}/zsh-plugins/oh-my-zsh
    cp ${configsFolder}/zsh/konno-yuuki.zsh-theme ${configsRoot}/zsh-plugins/oh-my-zsh/themes
  fi
  # Installing zsh auto suggestions
  if [[ ! -d ${configsRoot}/zsh-plugins/zsh-auto-suggestions ]]; then
    $git clone https://github.com/zsh-users/zsh-autosuggestions ${configsRoot}/zsh-plugins/zsh-auto-suggestions 
  fi
  echo "Rendering zshrc from template"
  cp ${configsFolder}/zsh/zshrc $HOME/.zshrc
  sed -i "s|\$CHANGEME|${configsRoot}\/zsh-plugins|g" $HOME/.zshrc  
  echo "Symlinking aliases"
  ln -sf ${configsFolder}/zsh/aliases $HOME/.aliases
}

############################################
#################Packages###################
############################################
function pkg-debian { 
  pkglist= $(cat ${configsFolder}/packages/debian)
  echo "Installing debian specific packages with apt"
  sudo apt install ${pkglist} -y
}
function pkg-macos {
  echo "Installing macOS specific packages with brew"
  pkglist= $(cat ${configsFolder}/packages/macos)
  sudo brew install $pkglist -y

############################################
####################Git#####################
############################################
}
function git {
  echo "Configuring git"
  ln -sf ${configsFolder}/git/gitconfig $HOME/.gitconfig
}
function configs {
  echo "Welcome to the script"
}

if [[ ${all} == "true" ]]; then
  echo "Launching in ${configsFolder}"
  for part in $(find ${configsFolder} -maxdepth 1 -type d | grep -v "/.git\|packages"); do
    $(basename ${part})
  done
elif [[ ${install_only} ]]; then
  for part in ${install_only}; do
    $part
  done
fi
